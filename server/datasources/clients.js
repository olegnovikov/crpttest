const clients = [{
    id: 1,
    name: 'АО Мегафон',
    inn: 4837027920
}, {
    id: 2,
    name: 'АО Вымпелком',
    inn: 212327920
}, {
    id: 3,
    name: 'АО МТС',
    inn: 564927920
}, {
    id: 4,
    name: 'АО Ростелеком',
    inn: 964967920
}, {
    id: 5,
    name: 'АО КАРИ',
    inn: 700967920
}, {
    id: 6,
    name: 'АО ЗЕНДЕН',
    inn: 830967920
}, {
    id: 7,
    name: 'АО Детский мир',
    inn: 370967920
}]

module.exports = clients;