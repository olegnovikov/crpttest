const {Router} = require('express');
const router = Router();

const moment = require('moment');

const report = require('../datasources/report');

const getResponse = require('../utils');

router.post('/', async (req, res) => {
    try {
        setTimeout(() => {
            const {startDate, endDate} = req.body;
            res.status(200).json(getResponse({
                    data: report.filter(r =>
                        moment(r.date).startOf('day') >= moment(startDate).startOf('day') &&
                        moment(r.date).startOf('day') <= moment(endDate).startOf('day')
                    )
                }
            ));
        }, 3000);
        // throw('');
    } catch (e) {
        res.status(500).json(getResponse({error: 'Ошибка получения данных' }))
    }
})

module.exports = router