import { call, put, takeEvery } from 'redux-saga/effects'
import {
    CLEAR_ACCOUNTS,
    GET_ACCOUNTS,
    GET_ACCOUNTS_ERROR,
    GET_ACCOUNTS_LOADING,
    GET_ACCOUNTS_SUCCESS
} from '../actions/action-types';

async function fetchAccounts(payload: any) {
    const response = await fetch('/api/accounts', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({...payload})
    });
    return await response.json();
}

function* getAccounts(action: any) {
    try {
        yield put({ type: GET_ACCOUNTS_LOADING});
        const accounts = yield call(fetchAccounts, action.payload);
        if (accounts.error) {
            yield put({ type: GET_ACCOUNTS_ERROR, payload: accounts.error });
        } else {
            yield put({ type: GET_ACCOUNTS_SUCCESS, payload: accounts.data });
        }
    } catch (e) {
        yield put({ type: GET_ACCOUNTS_ERROR, payload: 'Ошибка получения данных'});
    }
}

function* clearAccounts() {
    yield put({ type: GET_ACCOUNTS_SUCCESS, payload: [] });
}

export function* getAccountsWatch() {
    yield takeEvery(GET_ACCOUNTS, getAccounts);
    yield takeEvery(CLEAR_ACCOUNTS, clearAccounts);
}
