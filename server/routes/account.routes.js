const {Router} = require('express')
const router = Router()

const accounts = require('../datasources/accounts');

const getResponse = require('../utils');

router.post('/', async (req, res) => {
    try {
        const {search, client} = req.body;
        let data;
        if (search) {
            data = accounts.filter(a =>  a.client_id === client.id && a.number.indexOf(search) >= 0);
        } else {
            data = accounts.filter(a => a.client_id === client.id);
        }
        res.status(200).json(getResponse({data}))
    } catch (e) {
        res.status(500).json(getResponse({error: 'Ошибка получения данных' }))
    }
})

module.exports = router