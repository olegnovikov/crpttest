import {GET_REPORT_ERROR, GET_REPORT_LOADING, GET_REPORT_SUCCESS} from '../actions/action-types';
import {IStoreReport} from '../../models/state';
import {IReport} from '../../models/report';

const initialState: IStoreReport<IReport> = {
    report: {
        loading: false,
        data: [],
        error: null
    }
}

export const reportReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case GET_REPORT_LOADING:
            return {
                ...state, report: {
                    ...state.report,
                    loading: true,
                    data: [],
                    error: null
                }
            }
        case GET_REPORT_SUCCESS:
            return {
                ...state, report: {
                    ...state.report,
                    loading: false,
                    data: action.payload,
                    error: null
                }
            }
        case GET_REPORT_ERROR:
            return {
                ...state, report: {
                    ...state.report,
                    loading: false,
                    data: [],
                    error: action.payload
                }
            }
        default:
            return state
    }
}