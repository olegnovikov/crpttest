import { call, put, takeEvery } from 'redux-saga/effects'
import {
    GET_CLIENTS,
    GET_CLIENTS_ERROR,
    GET_CLIENTS_LOADING,
    GET_CLIENTS_SUCCESS
} from '../actions/action-types';

async function fetchClients(search: string) {
    const response = await fetch('/api/clients', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: search && JSON.stringify({search})
    });
    return await response.json();
}

function* getClients(action: any) {
    try {
        yield put({ type: GET_CLIENTS_LOADING});
        const clients = yield call(fetchClients, action.payload);
        if (clients.error) {
            yield put({ type: GET_CLIENTS_ERROR, payload: clients.error });
        } else {
            yield put({ type: GET_CLIENTS_SUCCESS, payload: clients.data });
        }
    } catch (e) {
        yield put({ type: GET_CLIENTS_ERROR, payload: 'Ошибка получения данных'});
    }
}

export function* getClientsWatch() {
    yield takeEvery(GET_CLIENTS, getClients);
}
