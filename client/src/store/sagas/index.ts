import {all} from 'redux-saga/effects'
import {getClientsWatch} from './clients';
import {getAccountsWatch} from './accounts';
import {getReportWatch} from './report';

export function* rootSaga() {
    yield all([
        getClientsWatch(),
        getAccountsWatch(),
        getReportWatch()
    ])
}
