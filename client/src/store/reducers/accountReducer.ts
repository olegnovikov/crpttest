import {GET_ACCOUNTS_ERROR, GET_ACCOUNTS_LOADING, GET_ACCOUNTS_SUCCESS} from '../actions/action-types';
import {IStoreAccounts} from '../../models/state';
import {IAccount} from '../../models/account';

const initialState: IStoreAccounts<IAccount> = {
    accounts: {
        loading: false,
        data: [],
        error: null
    }
}

export const accountReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case GET_ACCOUNTS_LOADING:
            return {
                ...state, accounts: {
                    ...state.accounts,
                    loading: true,
                    data: [],
                    error: null
                }
            }
        case GET_ACCOUNTS_SUCCESS:
            return {
                ...state, accounts: {
                    ...state.accounts,
                    loading: false,
                    data: action.payload,
                    error: null
                }
            }
        case GET_ACCOUNTS_ERROR:
            return {
                ...state, accounts: {
                    ...state.accounts,
                    loading: false,
                    error: action.payload
                }
            }
        default:
            return state
    }
}
