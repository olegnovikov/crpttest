import {GET_CLIENTS_ERROR, GET_CLIENTS_LOADING, GET_CLIENTS_SUCCESS} from '../actions/action-types';
import {IStoreClients} from '../../models/state';
import {IClient} from '../../models/client';

const initialState: IStoreClients<IClient> = {
    clients: {
        loading: false,
        data: [],
        error: null
    }
}

export const clientReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case GET_CLIENTS_LOADING:
            return {
                ...state, clients: {
                    ...state.clients,
                    loading: true,
                    data: [],
                    error: null
                }
            }
        case GET_CLIENTS_SUCCESS:
            return {
                ...state, clients: {
                    ...state.clients,
                    loading: false,
                    data: action.payload,
                    error: null
                }
            }
        case GET_CLIENTS_ERROR:
            return {
                ...state, clients: {
                    ...state.clients,
                    loading: false,
                    error: action.payload
                }
            }
        default:
            return state
    }
}
