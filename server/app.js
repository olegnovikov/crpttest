const express = require('express');
const path = require('path');

const app = express();

app.use(express.json({ extended: true }));

app.use('/api/clients', require('./routes/client.routes'));
app.use('/api/accounts', require('./routes/account.routes'));
app.use('/api/report', require('./routes/report.routes'));

// if (process.env.NODE_ENV === 'production') {
//     app.use('/', express.static(path.join(__dirname, 'client', 'build')))
//
//     app.get('*', (req, res) => {
//         res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
//     })
// }

const PORT = 5000;

app.listen(PORT, () => console.log(`App has been started on port ${PORT}...`));