const {Router} = require('express')
const router = Router()

const clients = require('../datasources/clients');

const getResponse = require('../utils');

router.post('/', async (req, res) => {
    try {
        const {search} = req.body;
        let data;
        if (search) {
            data = clients.filter(cl => cl.name.toLowerCase().indexOf(search.toLowerCase()) >= 0);
        } else {
            data = clients.slice(0, 3);
        }
        res.status(200).json(getResponse({data}))
    } catch (e) {
        res.status(500).json(getResponse({error: 'Ошибка получения данных' }))
    }
})

module.exports = router
