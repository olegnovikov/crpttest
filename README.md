This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Тестовое приложение для ЦРПТ

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm server`
Runs the server in the development mode.

### `npm client`
Runs the client in the development mode.