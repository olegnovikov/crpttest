export interface IReport {
    id: number;
    date: number;
    operation: IOperation;
    balance: number;
}

interface IOperation {
    number: number;
    type: string;
    name: string;
    description: string;
    payment: number;
    charge: number;
}
