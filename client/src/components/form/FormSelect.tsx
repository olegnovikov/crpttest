import React, {FC} from 'react';
import 'moment/locale/ru';
import {
    CircularProgress,
    TextField
} from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';

import debounce from 'lodash/debounce';

interface Props {
    label: string;
    value: any;
    loading: boolean;
    disabled?: boolean;
    options: any[];
    getOptionLabel: (option: any) => string;
    onSearchChange: Function;
    onChange: Function;
}

export const FormSelect: FC<Props> = (props: Props) => {
    const {label, value, loading, options, getOptionLabel, disabled = false} = props;

    const [open, setOpen] = React.useState(false);

    const debounceInputChange = debounce((value) => {
        props.onSearchChange(value);
    }, 500)

    const handleInputChange = (e: any,  value: any) => {
        debounceInputChange(value);
    }

    const handleChange = (e: any, value: any) => {
        props.onChange(value);
    }

    return (
        <Autocomplete
            open={open && (!loading && options.length > 0)}
            value={value}
            onOpen={() => {
                setOpen(true);
            }}
            onClose={() => {
                setOpen(false);
            }}
            onChange={handleChange}
            onInputChange={handleInputChange}
            getOptionSelected={(option: any, value) => option.id === value.id}
            getOptionLabel={getOptionLabel}
            options={options}
            loading={loading}
            disabled={disabled}
            renderInput={(params) => (
                <TextField
                    {...params}
                    label={label}
                    variant="outlined"
                    InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                            <>
                                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                                {params.InputProps.endAdornment}
                            </>
                        ),
                    }}
                />
            )}
        />
    )
}