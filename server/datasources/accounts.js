const accounts = [{
    id: 1,
    client_id: 1,
    group: 'TГ Связь',
    number: '40702810500000000001'
}, {
    id: 2,
    client_id: 2,
    group: 'TГ Связь',
    number: '40702810500000000002'
}, {
    id: 3,
    client_id: 3,
    group: 'TГ Связь',
    number: '40702810500000000003'
}, {
    id: 4,
    client_id: 4,
    group: 'TГ Связь',
    number: '40702810500000000004'
}, {
    id: 5,
    client_id: 5,
    group: 'TГ Обувь',
    number: '40702810500000000005'
}, {
    id: 6,
    client_id: 6,
    group: 'TГ Обувь',
    number: '40702810500000000006'
}, {
    id: 7,
    client_id: 7,
    group: 'TГ Обувь',
    number: '40702810500000000007'
}, {
    id: 8,
    client_id: 1,
    group: 'TГ Связь',
    number: '40702810500000000008'
}, {
    id: 9,
    client_id: 2,
    group: 'TГ Связь',
    number: '40702810500000000009'
}, {
    id: 10,
    client_id: 3,
    group: 'TГ Связь',
    number: '40702810500000000010'
}, {
    id: 11,
    client_id: 4,
    group: 'TГ Связь',
    number: '40702810500000000011'
}, {
    id: 12,
    client_id: 5,
    group: 'TГ Обувь',
    number: '40702810500000000012'
}, {
    id: 13,
    client_id: 6,
    group: 'TГ Обувь',
    number: '40702810500000000013'
}, {
    id: 14,
    client_id: 7,
    group: 'TГ Обувь',
    number: '40702810500000000014'
}]

module.exports = accounts;