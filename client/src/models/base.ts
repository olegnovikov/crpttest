export interface IAsyncData<T> {
    loading: boolean;
    data: T | T[];
    error: string | null;
}