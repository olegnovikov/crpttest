import {combineReducers} from 'redux';
import {clientReducer} from './clientReducer';
import {accountReducer} from './accountReducer';
import {reportReducer} from './reportReducer';

export const rootReducer = combineReducers({
    clients: clientReducer,
    accounts: accountReducer,
    report: reportReducer
})