import React from 'react';
import {createMuiTheme, CssBaseline} from '@material-ui/core';
import {grey} from '@material-ui/core/colors';
import {ThemeProvider} from '@material-ui/styles';
import {AccountRegisterPage} from '../pages/AccountRegisterPage';
import {MainLayout} from './layouts/MainLayout';
import {MuiPickersUtilsProvider} from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import { ruRU } from '@material-ui/core/locale';
import 'moment/locale/ru';

/**
 * Главный компонет приложения
 */
const theme = createMuiTheme({
    palette: {
        primary: { main: grey[500] },
        secondary: { main: grey[100]},
    },
}, ruRU);

function App() {
  return (
      <ThemeProvider theme={theme}>
          <CssBaseline/>
          <MuiPickersUtilsProvider utils={MomentUtils} locale="ru">
              <MainLayout>
                  <AccountRegisterPage/>
              </MainLayout>
          </MuiPickersUtilsProvider>
      </ThemeProvider>
  );
}

export default App;
