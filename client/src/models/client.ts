export interface IClient {
    id: number;
    name: string;
    inn: number;
}
