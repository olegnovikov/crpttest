const report = [{
    id: 1,
    date: 1590796800000,
    operation: {
        number: 1,
        type: 'Платеж',
        name: 'Инициализация лицевого счета',
        description: 'Документ №1',
        payment: 8000,
        charge: 0,
    },
    balance: 8000
}, {
    id: 2,
    date: 1590796800000,
    operation: {
        number: 1,
        type: 'Платеж',
        name: 'Пополнение лицевого счета',
        description: 'Документ №2',
        payment: 2000,
        charge: 0,
    },
    balance: 10000
}, {
    id: 3,
    date: 1590883200000,
    operation: {
        number: 1,
        type: 'Списание',
        name: 'Списание с лицевого счета',
        description: 'Документ №3',
        payment: 0,
        charge: 5000,
    },
    balance: 5000
}]

module.exports = report;