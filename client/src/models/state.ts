import {IAsyncData} from './base';
import {IClient} from './client';
import {IAccount} from './account';
import {IReport} from './report';

export interface IStoreClients<T> {
    clients: IAsyncData<T>
}

export interface IStoreAccounts<T> {
    accounts: IAsyncData<T>
}

export interface IStoreReport<T> {
    report: IAsyncData<T>
}

export interface IStoreApp {
    clients: IStoreClients<IClient>;
    accounts: IStoreAccounts<IAccount>;
    report: IStoreReport<IReport>;
}