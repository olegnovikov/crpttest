import { call, put, takeEvery } from 'redux-saga/effects'
import {
    CLEAR_REPORT,
    GET_REPORT,
    GET_REPORT_ERROR,
    GET_REPORT_LOADING,
    GET_REPORT_SUCCESS
} from '../actions/action-types';

async function fetchReport(payload: any) {
    const response = await fetch('/api/report', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({...payload})
    });
    return await response.json();
}

function* getReport(action: any) {
    try {
        yield put({ type: GET_REPORT_LOADING});
        const report = yield call(fetchReport, action.payload);
        if (report.error) {
            yield put({ type: GET_REPORT_ERROR, payload: report.error });
        } else {
            yield put({ type: GET_REPORT_SUCCESS, payload: report.data });
        }
    } catch (e) {
        yield put({ type: GET_REPORT_ERROR, payload: 'Ошибка получения данных'})
    }
}

function* clearReport() {
    yield put({ type: GET_REPORT_SUCCESS, payload: [] });
}

export function* getReportWatch() {
    yield takeEvery(GET_REPORT, getReport);
    yield takeEvery(CLEAR_REPORT, clearReport);
}
