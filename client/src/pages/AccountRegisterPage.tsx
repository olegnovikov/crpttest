import React, {FC, useEffect, useState} from 'react';
import {KeyboardDatePicker} from '@material-ui/pickers';
import moment from 'moment';
import isEmpty from 'lodash/isEmpty';
import values from 'lodash/values';
import every from 'lodash/every';
import {
    Box,
    CircularProgress,
    Grid,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from '@material-ui/core';

import {useDispatch, useSelector} from 'react-redux';
import {FormSelect} from '../components/form/FormSelect';
import {CLEAR_ACCOUNTS, CLEAR_REPORT, GET_ACCOUNTS, GET_CLIENTS, GET_REPORT} from '../store/actions/action-types';
import {IClient} from '../models/client';
import {IAccount} from '../models/account';
import {IReport} from '../models/report';
import {IStoreApp} from '../models/state';

interface IState {
    startDate: moment.Moment | null;
    endDate: moment.Moment | null;
    client: IClient | null;
    account: IAccount | null;
};

export const AccountRegisterPage: FC = () => {
    const dispatch = useDispatch();
    const [form, setForm] = useState<IState>({startDate: null, endDate: null, client: null, account: null});

    const clients = useSelector((state: IStoreApp) => state.clients.clients);
    const accounts = useSelector((state: IStoreApp) => state.accounts.accounts);
    const report = useSelector((state: IStoreApp) => state.report.report);

    useEffect(() => {
        dispatch({type: GET_CLIENTS});
    }, [dispatch])

    const handleChangeForm = (field: keyof IState) => (event: any) => {
        const newForm = {...form, [field]: event && event.target ? event.target.value : event};
        setForm(newForm);
        if (every(values(newForm), f => !!f)) {
            formSubmit(newForm);
        } else {
            dispatch({type: CLEAR_REPORT});
        }
    };

    const handleChangeFormClient = (value: IClient) => {
        let newForm = {...form, client: value };
        if (!isEmpty(value)) {
            dispatch({type: GET_ACCOUNTS, payload: {client: value}});
        } else {
            dispatch({type: CLEAR_ACCOUNTS});
            newForm.account = null;
        }
        setForm(newForm);
    }

    const handleChangeClient = (value: string) => {
        if (value) {
            if (value.length > 2) {
                dispatch({type: GET_CLIENTS, payload: value});
            }
        } else {
            dispatch({type: GET_CLIENTS});
        }
    }

    const handleChangeAccount = (value: string) => {
        if (form.client) {
            if (value) {
                if (value.length > 2) {
                    dispatch({type: GET_ACCOUNTS, payload: {client: form.client, search: value}});
                }
            } else {
                dispatch({type: GET_ACCOUNTS, payload: {client: form.client}});
            }
        }
    }

    const formSubmit = (payload: IState) => {
        dispatch({type: GET_REPORT, payload});
    }

    return (
        <div>
            <Grid container spacing={4}>
                <Grid item xs={3}>
                    <Box borderLeft={4} borderColor="success.main" borderRadius={6}>
                        <KeyboardDatePicker
                            autoOk
                            variant="inline"
                            inputVariant="outlined"
                            label="Дата начала периода"
                            format="DD.MM.YYYY"
                            value={form.startDate}
                            InputAdornmentProps={{position: 'end'}}
                            onChange={handleChangeForm('startDate')}
                        />
                    </Box>
                </Grid>
                <Grid item xs={3}>
                    <Box borderLeft={4} borderColor="success.main" borderRadius={6}>
                        <KeyboardDatePicker
                            autoOk
                            variant="inline"
                            inputVariant="outlined"
                            label="Дата окончания периода"
                            format="DD.MM.YYYY"
                            value={form.endDate}
                            InputAdornmentProps={{position: 'end'}}
                            onChange={handleChangeForm('endDate')}
                        />
                    </Box>
                </Grid>
                <Grid item xs={3}>
                    <Box borderLeft={4} borderColor="success.main" borderRadius={6}>
                        <FormSelect
                            label="Участник оборота"
                            value={form.client}
                            loading={clients.loading}
                            options={clients.data as any[]}
                            getOptionLabel={(option) => `${option.name} ИНН: ${option.inn}`}
                            onChange={handleChangeFormClient}
                            onSearchChange={handleChangeClient}
                        />
                    </Box>
                </Grid>
                <Grid item xs={3}>
                    <Box borderLeft={4} borderColor="success.main" borderRadius={6}>
                        <FormSelect
                            label="Лицевой счет"
                            value={form.account}
                            loading={accounts.loading}
                            options={accounts.data as any[]}
                            disabled={isEmpty(form.client)}
                            getOptionLabel={(option) => `${option.group} ${option.number}`}
                            onChange={handleChangeForm('account')}
                            onSearchChange={handleChangeAccount}
                        />
                    </Box>
                </Grid>
            </Grid>
            <Box marginTop={4}>
                {report.loading
                    ? <Box display="flex" justifyContent="center"><CircularProgress color="inherit" size={40}/></Box>
                    : report.error
                    ? <Box display="flex" justifyContent="center" color="error.main">{report.error}</Box>
                    : <TableContainer component={Paper}>
                        <Table aria-label="report table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Дата</TableCell>
                                    <TableCell align="right">Номер операции<br/>Тип</TableCell>
                                    <TableCell align="right">Наименование операции<br/>Детали операции</TableCell>
                                    <TableCell align="right">Поступление</TableCell>
                                    <TableCell align="right">Списание</TableCell>
                                    <TableCell align="right">Баланс счета</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {isEmpty(report.data)
                                    ?  <TableRow><TableCell colSpan={6} align="center">Нет записей</TableCell></TableRow>
                                    :  (report.data as IReport[]).map((row: IReport) => (
                                    <TableRow key={row.id}>
                                        <TableCell align="left">{moment(row.date).format('DD.MM.YYYY')}</TableCell>
                                        <TableCell align="right">{row.operation.number}<br/>{row.operation.type}
                                        </TableCell>
                                        <TableCell align="right">{row.operation.name}<br/>{row.operation.description}
                                        </TableCell>
                                        <TableCell align="right">{row.operation.payment}</TableCell>
                                        <TableCell align="right">{row.operation.charge}</TableCell>
                                        <TableCell align="right">{row.balance}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                }
            </Box>
        </div>
    )
}
