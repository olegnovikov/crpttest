export interface IAccount {
    id: number;
    client_id: number;
    group: string;
    number: string;
}