import React, {FC} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import {Drawer, Toolbar} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex'
    },
    appBar: {
        height: 60,
        paddingLeft: 80,
        boxShadow: 'none',
        backgroundColor: theme.palette.secondary.main
    },
    appBarSpacer: theme.mixins.toolbar,
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: 80
    },
    title: {
        flexGrow: 1
    },
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto'
    },
    mainContent: {
        padding: theme.spacing(4)
    }
}));

export const MainLayout: FC = (props) => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar position="absolute" className={classes.appBar}>
                <Box borderBottom={1} borderColor="secondary.main">
                    <Toolbar>
                        <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                            Тестовое задание
                        </Typography>
                    </Toolbar>
                </Box>
            </AppBar>
            <Drawer
                variant="permanent"
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
            </Drawer>
            <main className={classes.content}>
                <div className={classes.appBarSpacer} />
                <div className={classes.mainContent}>
                    {props.children}
                </div>
            </main>
        </div>
    );
}